package structure;

import java.util.ArrayList;
import java.util.HashMap;

public class Router {

	private static int id=-1;
	public String name;
	private HashMap<Router,Integer> links=new HashMap<>();
	
	public Router() {
		// TODO Auto-generated constructor stub
		id++;
		this.name=""+id;
	}

	public void resetLinks(){
		this.links=new HashMap<>();
	}
	
	public void buildLink(Router router, int weight){
		this.links.remove(router);
		router.removeLink(this);

		
		this.links.put(router, weight);
		router.addLink(this, weight);
	}
	
	public void addLink(Router router, int weight){
		this.links.put(router, weight);
	}
	
	public int getWeight(Router router){
		return this.links.get(router);
	}

	public HashMap<Router, Integer> getLinks() {
		return links;
	}

	public void removeLink(Router router){
		this.links.remove(router);
	}
	
	public void setLinks(HashMap<Router, Integer> links) {
		this.links = links;
	}
}
