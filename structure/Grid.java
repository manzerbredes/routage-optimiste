package structure;

import java.util.*;
import java.util.Map.Entry;

public class Grid {

	public enum Protocol {
	    AODV, DSDV, CUSTOM
	}
	
	private ArrayList<Router> routers=new ArrayList<>();
	private ArrayList<ArrayList<Integer>> links=new ArrayList<>();
	
	
	private int bestLink;
	private Protocol protocol;
	private int counterCUSTOM=5;
	private Random rand = new Random();
	private final int maxWeight=100;
	
	
	
	
	
	/**
	 * Build a 3x3 Grid
	 */
	public Grid(Protocol protocol){
			
		// Build router liste
		for(int i=0;i<9;i++){
			this.routers.add(new Router());
		}
		
		
		this.buildEdgeWithRandomWeigth();
		
		this.buildPath();
		
		
		this.protocol=protocol;

		switch(protocol){
			case AODV:
				this.bestLink=this.getBestLinkIndex();
				break;
			case DSDV: 
			case CUSTOM:
				HashMap<Integer,Integer> currentBestLink=new HashMap<>();
				for(int i=0;i<100000;i++){
					int current=this.getBestLinkIndex();
					if(currentBestLink.containsKey(current)){
						currentBestLink.put(current, currentBestLink.get(current)+1);
					}
					else{
						currentBestLink.put(current, 1);
					}
					this.buildEdgeWithRandomWeigth();
				}
				Set<Integer> entryTMP = currentBestLink.keySet();
				
				int max=currentBestLink.get(entryTMP.iterator().next());
				int maxId=0;
				entryTMP = currentBestLink.keySet();
				Iterator<Integer> it=entryTMP.iterator();
				while(it.hasNext()) {
					int entryId=it.next();
					int entry=currentBestLink.get(entryId);
					if(entry> max){
						max=entry;
						maxId=entryId;
					}
					
					System.out.println("Id : "+ entryId + " max "+ entry);
					
				}
				this.bestLink=maxId;
				System.out.println("Retenu :"+maxId);
				break;
			
				
		}
		
	}
	
	
	
	public void buildEdgeWithRandomWeigth(){
		// First line
		this.buildLinkWithRandomWeight(routers.get(0), routers.get(1));
		this.buildLinkWithRandomWeight(routers.get(1), routers.get(2));

		// Second line
		this.buildLinkWithRandomWeight(routers.get(3), routers.get(4));
		this.buildLinkWithRandomWeight(routers.get(4), routers.get(5));
		
		// Third line
		this.buildLinkWithRandomWeight(routers.get(6), routers.get(7));
		this.buildLinkWithRandomWeight(routers.get(7), routers.get(8));
		
		// First column
		this.buildLinkWithRandomWeight(routers.get(0), routers.get(3));
		this.buildLinkWithRandomWeight(routers.get(3), routers.get(6));
		
		// Second column
		this.buildLinkWithRandomWeight(routers.get(1), routers.get(4));
		this.buildLinkWithRandomWeight(routers.get(4), routers.get(7));
		
		// Third column
		this.buildLinkWithRandomWeight(routers.get(2), routers.get(5));
		this.buildLinkWithRandomWeight(routers.get(5), routers.get(8));

	}
	
	private void buildPath(){
		
		// Link1
		ArrayList<Integer> link1=new ArrayList<>();
		link1.add(0);link1.add(1);link1.add(2);link1.add(5);link1.add(4);link1.add(3);link1.add(6);link1.add(7);link1.add(8);
		this.links.add(link1);
		
		// Link2
		ArrayList<Integer> link2=new ArrayList<>();
		link2.add(0);link2.add(1);link2.add(2);link2.add(5);link2.add(4);link2.add(7);link2.add(8);
		this.links.add(link2);
		
		// link3
		ArrayList<Integer> link3=new ArrayList<>();
		link3.add(0);link3.add(1);link3.add(2);link3.add(5);link3.add(8);
		this.links.add(link3);
		
		// link4
		ArrayList<Integer> link4=new ArrayList<>();
		link4.add(0);link4.add(1);link4.add(4);link4.add(5);link4.add(8);
		this.links.add(link4);
		
		// link5
		ArrayList<Integer> link5=new ArrayList<>();
		link5.add(0);link5.add(3);link5.add(4);link5.add(5);link5.add(8);
		this.links.add(link5);
		
		// link6
		ArrayList<Integer> link6=new ArrayList<>();
		link6.add(0);link6.add(3);link6.add(4);link6.add(7);link6.add(8);
		this.links.add(link6);
		
		// link7
		ArrayList<Integer> link7=new ArrayList<>();
		link7.add(0);link7.add(3);link7.add(6);link7.add(7);link7.add(8);
		this.links.add(link7);
		
		// link8
		ArrayList<Integer> link8=new ArrayList<>();
		link8.add(0);link8.add(3);link8.add(6);link8.add(7);link8.add(4);link8.add(5);link8.add(8);
		this.links.add(link8);
		
		// link9
		ArrayList<Integer> link9=new ArrayList<>();
		link9.add(0);link9.add(3);link9.add(6);link9.add(7);link9.add(4);link9.add(1);link9.add(2);link9.add(5);link9.add(8);
		this.links.add(link9);
		
		// link10
		ArrayList<Integer> link10=new ArrayList<>();
		link10.add(0);link10.add(3);link10.add(4);link10.add(1);link10.add(2);link10.add(5);link10.add(8);
		this.links.add(link10);
		
		// link11
		ArrayList<Integer> link11=new ArrayList<>();
		link11.add(0);link11.add(1);link11.add(4);link11.add(3);link11.add(6);link11.add(7);link11.add(8);
		this.links.add(link11);
	}
	
	
	private void buildLinkWithRandomWeight(Router router1, Router router2){
		router1.buildLink(router2, rand.nextInt(this.maxWeight));
	}
	
	
	
	public int getBestLinkIndex(){
		int currentBestLink=0;
		int currentBestLinkBottleneck=0;
		for(int i=0;i<this.links.size();i++){
			ArrayList<Integer> currentLink=this.links.get(i);
			int currentLinkBottleneck=this.getMaxBottleneck(currentLink);
			if(currentBestLinkBottleneck<currentLinkBottleneck){
				currentBestLink=i;
				currentBestLinkBottleneck=currentLinkBottleneck;
			}
		}
		
		return currentBestLink;
	}
	

	private int getMaxBottleneck(ArrayList<Integer> link){
		int max=this.getWeigthOfLink(link.get(0), link.get(1));
		for(int j=1;j<link.size()-1;j++){
			int currentMax=this.getWeigthOfLink(link.get(j), link.get(j+1));
			if(max>currentMax){
				max=currentMax;
			}
			
		}
		return max;
	}
	
	private int getWeigthOfLink(int router1,int router2){
		return this.routers.get(router1).getWeight(this.routers.get(router2));
	}
	
	
	public void printLinkWeight(){
		for(int i=0;i<this.links.size();i++){
			ArrayList<Integer> link=this.links.get(i);
			System.out.print("Link number " + i + " ==> ");
			for(int j=0;j<link.size()-1;j++){
				System.out.print(this.getWeigthOfLink(link.get(j), link.get(j+1)) + "   ");
			}
			System.out.println("                                                       Goulot :"+this.getMaxBottleneck(link));
			//System.out.println();
		}
	}


	
	
	public boolean isEdgeOfLink(ArrayList<Integer>link, Router src, Router dest){
		for(int j=0;j<link.size()-1;j++){
			Router current=this.routers.get(link.get(j));
			if(src.name.equals(current.name)){
				if(j<link.size()-1){
					Router currentDest=this.routers.get(link.get(j+1));

					if(currentDest.name.equals(dest.name)){
						return true;
					}
				}
			}
		}
		for(int j=0;j<link.size()-1;j++){
			Router current=this.routers.get(link.get(j));
			if(dest.name.equals(current.name)){
				if(j<link.size()-1){
					Router currentDest=this.routers.get(link.get(j+1));

					if(currentDest.name.equals(src.name)){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	
	
	public ArrayList<Router> getGrid() {
		return routers;
	}


	public void setGrid(ArrayList<Router> grid) {
		this.routers = grid;
	}


	public ArrayList<ArrayList<Integer>> getLinks() {
		return links;
	}


	public void setLinks(ArrayList<ArrayList<Integer>> links) {
		this.links = links;
	}



	/**
	 * @return the bestLinkByProtocol
	 */
	public int getBestLinkByProtocol() {
		if(this.protocol==Protocol.CUSTOM){
			this.counterCUSTOM--;
			if(this.counterCUSTOM==0){
				this.bestLink=this.getBestLinkIndex();
				this.counterCUSTOM=5;
			}
			
		}
		return bestLink;
	}




	
	
}
