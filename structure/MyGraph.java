package structure;

import java.util.*;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.layout.springbox.EdgeSpring;
import org.graphstream.ui.swingViewer.basicRenderer.EdgeRenderer;
import org.graphstream.ui.util.EdgePoints;


public class MyGraph extends SingleGraph{

	
	private Grid grid;
	
	private int miss=0;
	private int success=0;
	
	public MyGraph(String title, Grid grid) {
		super(title);
		// Allow CSS on view
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		// Set graph CSS
		this.addAttribute("ui.stylesheet", "url('resources/style.css')");

		// Assign grid
		this.grid=grid;
		
		int i=0;
		int ll=this.grid.getGrid().size();
		// Build node
		for(Router r : this.grid.getGrid()){
			if(i==0){
				this.addNode(r.name).setAttribute("ui.label", "Source");
			}
			else{
				if(i==ll-1){
					this.addNode(r.name).setAttribute("ui.label", "Destination");

				}
				else{
					this.addNode(r.name);

				}

			}
			i++;
			
		}
		
		// Build Edges
		this.buildEdges();
		
		//Build bestLink
		this.showBestLink();
		
	}

	
	
	public void buildEdges(){
		
		/*Iterator<Edge> edges=this.getEdgeIterator();
		while(edges.hasNext()){
			Edge edge=edges.next();
			this.removeEdge(edge);
		}*/
		
		for(Router r : this.grid.getGrid()){
			
			 String current=r.name;
			
			
			 HashMap<Router, Integer> relier=r.getLinks();
			 Set<Router> k=relier.keySet();
			 Iterator<Router> i=k.iterator();
			 while(i.hasNext()){
				 Router currentRouter=i.next();
				 
				 String currentRouterName=currentRouter.name;
				 try{
						 Edge toAdd=this.addEdge(current+currentRouterName, current, currentRouterName);
						 toAdd.setAttribute("ui.label", relier.get(currentRouter));
				 }
				 catch(Exception e){
					// System.out.println("Bug de merde.");
				 }
				 
			 }
			 
		 }
	}
	
	public void showBestLink(){
		ArrayList<Integer> bestLink=this.grid.getLinks().get(this.grid.getBestLinkByProtocol());
		for(int i=0;i<bestLink.size();i++){
			Iterator<Node> nodes= this.getNodeIterator();
			while(nodes.hasNext()){
				Node node=nodes.next();
				Iterator<Edge> edges=node.getEdgeIterator();
				while(edges.hasNext()){
					Edge edge=edges.next();
					
					if(i<(bestLink.size()-1)){
						int destIndex=bestLink.get(i+1);
						String src=this.grid.getGrid().get(bestLink.get(i)).name;
						String dest=this.grid.getGrid().get(bestLink.get(i+1)).name;
						if((edge.getNode0().getId().equals(src) && edge.getNode1().getId().equals(dest))||(edge.getNode1().getId().equals(src) && edge.getNode0().getId().equals(dest))){
							edge.setAttribute("ui.style", "fill-color:red;");
						}
					}
					
				}
			}
		}
	}

	public void update(){
		// Reset color
		Iterator<Edge> edges=this.getEdgeIterator();
		while(edges.hasNext()){
			Edge edge=edges.next();
			edge.setAttribute("ui.style", "fill-color:black;");
		}
		// Update label
		edges=this.getEdgeIterator();
		while(edges.hasNext()){
			Edge edge=edges.next();
			for(Router r : this.grid.getGrid()){
				 String current=r.name;

				HashMap<Router, Integer> relier=r.getLinks();
				 Set<Router> k=relier.keySet();
				 Iterator<Router> i=k.iterator();
				 while(i.hasNext()){
					 Router currentRouter=i.next();
					 
					 String currentRouterName=currentRouter.name;
					 if(edge.getId().equals(current+currentRouterName)||edge.getId().equals(currentRouterName+current)){
						 edge.setAttribute("ui.label", relier.get(currentRouter));
					 }
				 }
			}
			
		}

		if(this.grid.getBestLinkByProtocol()==this.grid.getBestLinkIndex()){
			this.success++;
		}
		else{
			this.miss++;
		}
		System.out.println("Success = " + this.success + "  Miss = " + this.miss + " try number :"+(this.success+this.miss)) ;
		
		//Build bestLink
		this.showBestLink();
	}
	
}
