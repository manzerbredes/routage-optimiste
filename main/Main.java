package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.layout.springbox.EdgeSpring;
import org.graphstream.ui.swingViewer.basicRenderer.EdgeRenderer;
import org.graphstream.ui.util.EdgePoints;

import structure.Grid;
import structure.MyGraph;
import structure.Router;

public class Main {

	public static void main(String[] args) {
		Grid g=new Grid(Grid.Protocol.AODV);

		// Build Graph for graphstream
		MyGraph gr=new MyGraph("Routage Oportuniste", g);
		gr.display();
		
		
		// Update Graph
		while(true){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			g.buildEdgeWithRandomWeigth();
			System.out.println("Update !");
			gr.update();
		}

	}
}
