Routage Opportuniste
===================

This project is a graphical routing simulator. This project was design for proving that take care about radio conditions can improve **wifi** performance.


![Test](https://raw.githubusercontent.com/manzerbredes/routage-optimiste/master/resources/screen.png)

Library
-------------

This project was made with **GraphStream** to easily build graphical representation of the network (routers, links and weight).
